class BudgetsRepositoryImpl(
    private val budgetsApi: BudgetsApi,
    private val commonRepository: CommonRepository,
    private val sessionRepository: SessionRepositoryUpdater
) : BudgetsRepository {

    private val budgetCache = sessionRepository.cache(Update.Entity.BUDGET) {
        budgetsApi.getBudget().let {
            when (it.budget) {
                null -> null
                else -> with(it.budget) {
                    toDomainBudget(
                        billsSummary = it.billsSummary,
                        categoryBudgets = it.categoryBudgets,
                        categoryBudgetSuggestions = budgetsApi.getCategoryBudgetSuggestions(period.periodType, DateDto(period.startDate))
                    )
                }
            }
        }
    }

    override suspend fun loadDetectedIncomeTransactions(): List<Transaction> {
        return budgetsApi.getDetectedIncome().detectedIncome.transactions.toDomainTransactions(commonRepository)
    }

    override suspend fun loadCycleCashFlowReport(budgetCycle: BudgetCycle, startDate: Date): CycleCashFlowReport {
        return budgetsApi.getIncomeHistoryReport(budgetCycle.toPeriodType(), DateDto(startDate)).toDomainCycleCashFlowReport()
    }

    override suspend fun loadForecast(budgetCycle: BudgetCycle, startDate: Date): Budget.Forecast {
        return budgetsApi.getForecast(budgetCycle.toPeriodType(), DateDto(startDate)).toDomainForecast()
    }

    override suspend fun loadCategoryBudgetSuggestion(categoryId: Int, budgetCycle: BudgetCycle, startDate: Date): CategoryBudgetSuggestion {
        return budgetsApi.getCategoryBudgetSuggestions(budgetCycle.toPeriodType(), DateDto(startDate), categoryId).first().toDomainCategoryBudgetSuggestion()
    }

    override suspend fun loadCategoryBudgetSuggestions(budgetCycle: BudgetCycle, startDate: Date): List<CategoryBudgetSuggestion> {
        return budgetsApi.getCategoryBudgetSuggestions(budgetCycle.toPeriodType(), DateDto(startDate)).map { it.toDomainCategoryBudgetSuggestion() }
    }

    override suspend fun loadCategoryCycleCashFlowReport(categoryId: Int, budgetCycle: BudgetCycle, startDate: Date): CycleCashFlowReport {
        return budgetsApi.getIncomeHistoryReport(budgetCycle.toPeriodType(), DateDto(startDate), categoryId).toDomainCycleCashFlowReport()
    }

    override suspend fun loadBudget(fresh: Boolean): Budget? {
        if (fresh) budgetCache.reload()
        return budgetCache.getData()
    }

    override suspend fun updateBudget(budgetSetup: Budget.Setup) {
        budgetsApi.updateBudget(budgetSetup.toBudgetDtoSetup())
        sessionRepository.submitUpdate(Update.budget())
    }

    override suspend fun deleteBudget() {
        budgetsApi.deleteBudget()
        sessionRepository.submitUpdate(Update.budget())
    }

    private suspend fun BudgetDto.Forecast.toDomainForecast(): Budget.Forecast {
        return Budget.Forecast(
            currentSpend = transactionsTotal,
            bills = RecurringTransaction.Current(pastBills.toDomainRecurringTransactions(commonRepository), futureBills.toDomainRecurringTransactions(commonRepository)),
            billsTotal = pastBillsTotal + futureBillsTotal
        )
    }

    private suspend fun BudgetDto.TotalBudget.toDomainBudget(billsSummary: BudgetDto.BillsSummary, categoryBudgets: List<BudgetDto.CategoryBudget>, categoryBudgetSuggestions: List<BudgetDto.CategoryBudgetSuggestion>): Budget {
        val currentPeriod = Period(period.startDateForCurrentPeriod, period.endDateForCurrentPeriod)
        return Budget(
            initialStart = period.startDate,
            cycle = period.periodType.toBudgetCycle(),
            currentPeriod = currentPeriod,
            income = income,
            limit = limit,
            spent = spent,
            areBillsIncluded = areBillsIncluded,
            billsSummary = Budget.BillsSummary(
                currentPeriod = currentPeriod,
                limit = billsSummary.limit,
                spent = billsSummary.spent
            ),
            categoryBudgets = categoryBudgets.map {
                CategoryBudget(
                    category = commonRepository.getCategory(it.categoryId),
                    currentPeriod = currentPeriod,
                    limit = it.limit,
                    spent = it.spent
                )
            },
            categoryBudgetSuggestions = categoryBudgetSuggestions.map {
                it.toDomainCategoryBudgetSuggestion()
            }
        )
    }

    private suspend fun BudgetDto.CategoryBudgetSuggestion.toDomainCategoryBudgetSuggestion(): CategoryBudgetSuggestion {
        return CategoryBudgetSuggestion(
            category = commonRepository.getCategory(categoryId),
            suggestedLimit = suggestedLimit,
            spentCurrentCycle = spentCurrentCycle,
            spentLastCycle = spentLastCycle
        )
    }

    private fun Budget.Setup.toBudgetDtoSetup(): BudgetDto.Setup {
        return BudgetDto.Setup(
            budget = BudgetDto.Setup.TotalBudget(
                period = BudgetDto.Setup.Period(
                    periodType = cycle.toPeriodType(),
                    startDate = DateDto(start)
                ),
                income = income,
                limit = limit,
                areBillsIncluded = areBillsIncluded
            ),
            categoryBudgets = categoryBudgets.map {
                BudgetDto.Setup.CategoryBudget(
                    categoryId = it.first,
                    limit = it.second
                )
            }
        )
    }

}
