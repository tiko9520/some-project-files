

class AccountInsightsFragment : FragmentBase() {

    private val args: AccountInsightsFragmentArgs by navArgs()
    private val vm: AccountInsightsViewModel by viewModel { parametersOf(args.accountId) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return MainExploreAccountInsightsFragmentBinding.inflate(inflater).setup(vm) { binding ->
            binding.toolbar.setupWithNavController(findNavController())
            binding.appbar.applyCustomOutlineProvider()
            binding.list.apply {
                itemAnimator = null
                adapter = TransactionPagerListAdapter().also { adapter ->
                    val swipeController = SwipeController.forRecategorisation(context, binding.list, adapter::getTransaction, vm::recategorise)
                    addItemDecoration(SectionedListItemDecoration(requireContext()) { adapter.getItemDecoration(it) })
                    vm.transactionsPager.data.observe { adapter.submitData(viewLifecycleOwner.lifecycle, it); swipeController.reset() }
                }
            }
            vm.navigateToTransactionDetails.observeNonNull { navigate(AccountInsightsFragmentDirections.toTransactionDetails(it)) }
            vm.navigateToCategoryChange.observeNonNull { navigate(AccountInsightsFragmentDirections.toCategoryChange(longArrayOf(it.id), categoryId = it.category.root.id, isBulk = false)) }
        }
    }

}
