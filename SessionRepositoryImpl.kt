
interface SessionRepositoryUpdater {
    suspend fun submitUpdate(update: Update)
    fun registerForcedSignOut()
    fun registerNewSignUp()
    fun <T> cache(entity: Update.Entity? = null, images: (T) -> List<String>, query: suspend () -> T): Cache<T>
    fun <T> cache(entity: Update.Entity, query: suspend () -> T): Cache<T>
    suspend fun warmUpCaches()
    fun clear()
}

class SessionRepositoryImpl(private val application: Application, private val preferenceProvider: PreferenceProvider) : SessionRepository, SessionRepositoryUpdater {

    private var isSignUp = false
    private var isSignedOutForcedly = false
    private var foregroundSessionStart = Date()
    private val listeners = mutableListOf<(Update) -> Unit>()
    private val caches = mutableListOf<Pair<Cache<*>, Update.Entity?>>()

    init {
        application.registerReceiver(createGlobalRefreshTimeReceiver(), createGlobalRefreshTimeIntentFilter())
        application.registerActivityLifecycleCallbacks(createSessionLifecycleCallback())
    }

    override val isNewSignUp
        get() = isSignUp

    override val hadForcedSignOut: Boolean
        get() = isSignedOutForcedly

    override val foregroundSessionDuration: Duration
        get() = Duration.ofMillis(Date().time - foregroundSessionStart.time)

    override val signInSessionDuration: Duration
        get() = Duration.ofMillis(Date().time - preferenceProvider.get(PreferenceProvider.Keys.SIGN_IN_TIME, Date::class.java, Date()).time)

    override fun resetNewSignUpStatus() {
        isSignUp = false
    }

    override fun resetForcedSignOutStatus() {
        isSignedOutForcedly = false
    }

    override fun addUpdateListener(listener: (Update) -> Unit) {
        listeners.add(listener)
    }

    override fun removeUpdateListener(listener: (Update) -> Unit) {
        listeners.remove(listener)
    }

    override suspend fun submitUpdate(update: Update) {
        caches.forEach { if (update.entities.contains(it.second)) it.first.reload() }
        notifyUpdateListeners(update)
    }

    override fun registerForcedSignOut() {
        isSignedOutForcedly = true
    }

    override fun registerNewSignUp() {
        isSignUp = true
    }

    override fun <T> cache(entity: Update.Entity, query: suspend () -> T): Cache<T> {
        return cache(entity, { emptyList() }, query)
    }

    override fun <T> cache(entity: Update.Entity?, images: (T) -> List<String>, query: suspend () -> T): Cache<T> {
        return Cache(query, images).also { caches.add(it to entity) }
    }

    override suspend fun warmUpCaches() {
        caches.forEach {
            it.first.reload()
            preloadImages(it.first.getImagesToPreload())
        }
    }

    override fun clear() {
        resetNewSignUpStatus()
        resetForcedSignOutStatus()
        clearCaches()
        clearDiskCache()
    }

    private fun clearCaches() {
        caches.forEach { it.first.clear() }
    }

    private fun preloadImages(urls: Iterable<String>) {
        urls.distinct().forEach {
            Glide.with(application).load(it).diskCacheStrategy(DiskCacheStrategy.RESOURCE).preload()
        }
    }

    private fun clearDiskCache() {
        GlobalScope.launch(Dispatchers.IO) {
            application.cacheDir.deleteRecursively() // no need to call Glide's clearDiskCache since it is located in the same directory
        }
    }

    private fun restartForegroundSession() {
        foregroundSessionStart = Date()
    }

    private fun initiateGlobalRefresh() {
        MainScope().launch {
            clearCaches()
            warmUpCaches()
            notifyUpdateListeners(Update.all())
        }
    }

    private fun createGlobalRefreshTimeReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            private var lastTick = Date()
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action != Intent.ACTION_TIME_TICK || Date().truncate() != lastTick.truncate()) {
                    initiateGlobalRefresh()
                }
                lastTick = Date()
            }
        }
    }

    private fun createGlobalRefreshTimeIntentFilter(): IntentFilter {
        return IntentFilter().apply {
            // to handle user (or system in case of NTP updates) initialed time change
            addAction(Intent.ACTION_DATE_CHANGED)
            addAction(Intent.ACTION_TIMEZONE_CHANGED)
            addAction(Intent.ACTION_TIME_CHANGED)
            // to handle day change
            addAction(Intent.ACTION_TIME_TICK)
        }
    }

    private fun createSessionLifecycleCallback(): EmptyActivityLifecycleCallbacks {
        return object : EmptyActivityLifecycleCallbacks() {

            private val cacheTtl = TimeUnit.HOURS.toMillis(6) // cache lives 6 hours in background
            private val foregroundSessionTtl = TimeUnit.SECONDS.toMillis(5) // foreground session allows 5 seconds to switch between activities

            private var lastActive = SystemClock.elapsedRealtime()

            override fun onActivityResumed(activity: Activity) {
                val timeInBackground = SystemClock.elapsedRealtime() - lastActive
                if (timeInBackground > cacheTtl) {
                    initiateGlobalRefresh()
                }
                if (timeInBackground > foregroundSessionTtl) {
                    restartForegroundSession()
                }
            }

            override fun onActivityPaused(activity: Activity) {
                lastActive = SystemClock.elapsedRealtime()
            }

        }
    }

    private fun notifyUpdateListeners(update: Update) {
        listeners.forEach { it(update) }
    }

}
