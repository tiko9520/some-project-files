

interface BudgetsRepository {
    suspend fun loadDetectedIncomeTransactions(): List<Transaction>
    suspend fun loadCycleCashFlowReport(budgetCycle: BudgetCycle, startDate: Date): CycleCashFlowReport
    suspend fun loadForecast(budgetCycle: BudgetCycle, startDate: Date): Budget.Forecast
    suspend fun loadCategoryBudgetSuggestion(categoryId: Int, budgetCycle: BudgetCycle, startDate: Date): CategoryBudgetSuggestion
    suspend fun loadCategoryBudgetSuggestions(budgetCycle: BudgetCycle, startDate: Date): List<CategoryBudgetSuggestion>
    suspend fun loadCategoryCycleCashFlowReport(categoryId: Int, budgetCycle: BudgetCycle, startDate: Date): CycleCashFlowReport
    suspend fun loadBudget(fresh: Boolean): Budget?
    suspend fun updateBudget(budgetSetup: Budget.Setup)
    suspend fun deleteBudget()
}
