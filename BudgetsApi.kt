interface BudgetsApi {

    @GET("income/detect")
    suspend fun getDetectedIncome(): DetectedIncomeDto.Response

    @FormUrlEncoded
    @POST("budget/cashflow-history")
    suspend fun getIncomeHistoryReport(
        @Field("periodType") periodType: PeriodType,
        @Field("startDate") startDate: DateDto,
        @Field("userCategoryId") categoryId: Int? = null
    ): CycleCashFlowReportDto

    @FormUrlEncoded
    @POST("budget/forecast")
    suspend fun getForecast(
        @Field("periodType") periodType: PeriodType,
        @Field("startDate") startDate: DateDto
    ): BudgetDto.Forecast

    @FormUrlEncoded
    @POST("category/recommendations")
    suspend fun getCategoryBudgetSuggestions(
        @Field("periodType") periodType: PeriodType,
        @Field("startDate") startDate: DateDto,
        @Field("userCategoryId") categoryId: Int? = null
    ): List<BudgetDto.CategoryBudgetSuggestion>

    @GET("budget")
    suspend fun getBudget(): BudgetDto

    @POST("budget")
    suspend fun updateBudget(@Body budget: BudgetDto.Setup): BudgetDto

    @DELETE("budget")
    suspend fun deleteBudget()

}
