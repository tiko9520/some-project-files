
class TipsWidgetView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs), BindingView<ExploreViewModel> {

    private val binding = MainExploreViewTipsWidgetBinding.inflate(LayoutInflater.from(context), this, true).apply {
        clipChildren = false
    }

    override fun bind(lifecycleOwner: LifecycleOwner, viewModel: ExploreViewModel) {
        binding.bindTo(lifecycleOwner, viewModel)
        binding.list.apply {
            this.adapter = TipListAdapter().also { adapter ->
                val gap = resources.getDimensionPixelSize(R.dimen.spacing_2x)
                addItemDecoration(ItemOffsetDecoration(Rect(0, 0, gap, 0)) { !adapter.isLast(it) })
                viewModel.tipsList.observeNonNull(lifecycleOwner) { list ->
                    adapter.submitList(list)
                    scrollToPosition(list.indexOfFirst { !it.isCompleted })
                }
            }
        }
    }

}
