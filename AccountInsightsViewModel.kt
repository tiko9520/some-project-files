

class AccountInsightsViewModel(private val accountId: Int, private val accountsRepository: AccountsRepository) : ViewModelBase() {

    val navigateToTransactionDetails = LiveDataEvent<Long>()
    val navigateToCategoryChange = LiveDataEvent<Transaction>()

    val account = MutableLiveData<Account>()
    val bankType = MutableLiveData<Bank.Type>()
    val lastSynced = Transformations.map(account) { account -> account.lastSynced?.let { getString(R.string.label_synced_on_x, shortDayShortMonthYearDateFormat.format(it)) } ?: getString(R.string.label_never_synced) }
    val transactionsPager = TransactionPager(viewModelScope, ::handleError, ::showTransaction).also { isIdle.addBooleanSource(it.isLoading) }

    init {
        monitorUpdates(Update.Entity.ACCOUNTS, Update.Entity.TRANSACTION) { fetch() }
        fetch()
    }

    fun refresh() {
        fetch(fresh = true)
    }

    fun recategorise(transaction: Transaction) {
        navigateToCategoryChange.invoke(transaction)
    }

    private fun showTransaction(transaction: Transaction) {
        navigateToTransactionDetails.invoke(transaction.id)
    }

    private fun fetch(fresh: Boolean = false) {
        if (isLoading.value == true) {
            // most likely fetch has been caused by resyncAccount followed by Update.Entity.ACCOUNTS
            return
        }
        execute {
            if (fresh) accountsRepository.resyncAccounts(accountId) // will re-fetch account summary so no need to load fresh one after
            val accountSummary = accountsRepository.loadAccountSummary(fresh = false)
            val bank = accountSummary.findBank(accountId)!!
            setValue(account, bank.accounts.first { it.id == accountId })
            setValue(bankType, bank.type)
            transactionsPager.restart(
                query = { skip, take -> accountsRepository.loadAccountTransactions(Transaction.AccountListRequest(account.value!!.id, skip = skip, take = take)) },
                emptyStateViewModel = EmptyStateViewHolder.ViewModel(message = getString(R.string.label_no_transactions_available))
            )
        }
    }

}
