
val dataModule: Module = module(createdAtStart = true /* has to be created at start to register all caches for warm up */) {
    single { ApiFactory.createGson() }
    single<EnvironmentProvider> { EnvironmentProviderImpl() }
    single<InstrumentationProvider> { InstrumentationProviderImpl(get()) }
    single<AnalyticsProvider> { AnalyticsProviderImpl(get(), get()) }
    single<PreferenceProvider> { PreferenceProviderImpl(get(), get()) }
    single<ApiTokensProvider> { ApiTokensProviderImpl(get()) }
    single<BiometricProvider> { BiometricProviderImpl(get()) }
    single<PushNotificationsProvider> { PushNotificationsProviderImpl(get(), get(), get(), get(), get(), get()) }
    single<ImageProvider> { ImageProviderImpl(get(), get()) }
    single<ErrorHandler> { ErrorHandlerImpl(get(), get()) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), AuthenticationApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), BudgetsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), CategoriesApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), FilesApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), TransactionsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), AccountsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), ReportsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), UserProfileApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), BillsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), StaticApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), NotificationsApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), SurveyApi::class.java) }
    single { ApiFactory.createApi(get(), get(), get(), get(), get(), GoalsApi::class.java) }
    single<SessionRepository> { SessionRepositoryImpl(get(), get()) } bind SessionRepositoryUpdater::class
    single<CommonRepository> { CommonRepositoryImpl(get(), get(), get(), get()) }
    single<CurrentUserRepository> { CurrentUserRepositoryImpl(get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get()) }
    single<FundsTrackingRepository> { FundsTrackingRepositoryImpl(get(), get(), get(), get(), get()) }
    single<BudgetsRepository> { BudgetsRepositoryImpl(get(), get(), get()) }
    single<AccountsRepository> { AccountsRepositoryImpl(get(), get(), get(), get(), get(), get()) }
    single<BillsRepository> { BillsRepositoryImpl(get(), get(), get()) }
    single<EventRepository> { EventRepositoryImpl(get(), get()) }
    single<SurveyRepository> { SurveyRepositoryImpl(get()) }
    single<SettingsRepository> { SettingsRepositoryImpl(get()) }
    single<GoalsRepository> { GoalsRepositoryImpl(get(), get()) }
}
